// Schemas for TROVE zone structs have been intuited by inspection. No docs
// exist!
// Currently (PM V1.0) only Newspaper is supported.
var ZONE_NEWSPAPER = {
	id : 'newspaper',
	holder : 'article',
	dtag : 'date',
	rtag : 'relevance.score',
	stag : 'title.id',
	tags : [ {
		tag : 'id',
		title : 'ID',
		isLink : false
	}, {
		tag : 'date',
		title : 'Date',
		isLink : false
	}, {
		tag : 'title.value',
		title : 'Source',
		isLink : false
	}, {
		tag : 'category',
		title : 'Category',
		isLink : false
	}, {
		tag : 'heading',
		title : 'Heading',
		isLink : false
	}, {
		tag : 'relevance.value',
		title : 'Score',
		isLink : false
	}, {
		tag : 'relevance.score',
		title : 'Relevance',
		isLink : false
	}, {
		tag : 'page',
		title : 'Page',
		isLink : false
	}, {
		tag : 'snippet',
		title : 'Snippet',
		isLink : false
	}, {
		tag : 'text',
		title : 'Full Text',
		isLink : false
	}, {
		tag : 'troveUrl',
		title : 'URL',
		isLink : true
	} ]
};
var ZONE_ARTICLE = {
	id : 'article',
	holder : 'work',
	dtag : 'issued',
	rtag : 'relevance.score',
	stag : '',
	tags : [ {
		tag : 'id',
		title : 'ID',
		isLink : false
	}, {
		tag : 'title',
		title : 'Title',
		isLink : false
	}, {
		tag : 'issued',
		title : 'Date Issued',
		isLink : false
	}, {
		tag : 'isPartOf',
		title : 'Part of',
		isLink : false
	}, {
		tag : 'holdingsCount',
		title : 'Holding',
		isLink : false
	}, {
		tag : 'versionCount',
		title : 'Version',
		isLink : false
	}, {
		tag : 'relevance.value',
		title : 'Score',
		isLink : false
	}, {
		tag : 'relevance.score',
		title : 'Relevance',
		isLink : false
	}, {
		tag : 'type',
		title : 'Type',
		isLink : false,
		mayRepeat : true
	}, {
		tag : 'troveUrl',
		title : 'URL',
		isLink : true
	} ]
};
var ZONE_BOOK = {
	id : 'book',
	holder : 'work',
	dtag : 'issued',
	rtag : 'relevance.score',
	stag : '',
	tags : [ {
		tag : 'id',
		title : 'ID',
		isLink : false
	}, {
		tag : 'title',
		title : 'Title',
		isLink : false
	}, {
		tag : 'type',
		title : 'Type',
		isLink : false,
		mayRepeat : true
	}, {
		tag : 'issued',
		title : 'Date Issued',
		isLink : false
	}, {
		tag : 'contributor',
		title : 'Contributor(s)',
		isLink : false
	}, {
		tag : 'snippet',
		title : 'Snippet',
		isLink : false
	}, {
		tag : 'versionCount',
		title : 'Version',
		isLink : false
	}, {
		tag : 'relevance.value',
		title : 'Score',
		isLink : false
	}, {
		tag : 'relevance.score',
		title : 'Relevance',
		isLink : false
	}, {
		tag : 'troveUrl',
		title : 'URL',
		isLink : true
	} ]
};
var ZONE_PICTURE = { // FIXME: how to handle array of image links?
	id : 'picture',
	holder : 'work',
	dtag : 'issued',
	rtag : 'relevance.score',
	stag : '',
	tags : [ {
		tag : 'id',
		title : 'ID',
		isLink : false
	}, {
		tag : 'title',
		title : 'Title',
		isLink : false
	}, {
		tag : 'type',
		title : 'Media Type',
		isLink : false,
		mayRepeat : true
	}, {
		tag : 'issued',
		title : 'Date Issued',
		isLink : false
	}, {
		tag : 'snippet',
		title : 'Snippet',
		isLink : false
	}, {
		tag : 'holdingsCount',
		title : 'Holding',
		isLink : false
	}, {
		tag : 'versionCount',
		title : 'Version',
		isLink : false
	}, {
		tag : 'relevance.value',
		title : 'Score',
		isLink : false
	}, {
		tag : 'relevance.score',
		title : 'Relevance',
		isLink : false
	}, {
		tag : 'troveUrl',
		title : 'URL',
		isLink : true
	} ]
};
var ZONE_MAP = {
	id : 'map',
	holder : 'work',
	dtag : '',
	rtag : 'relevance.score',
	stag : '',
	tags : [ {
		tag : 'id',
		title : 'ID',
		isLink : false
	}, {
		tag : 'title',
		title : 'Title',
		isLink : false
	}, {
		tag : 'type',
		title : 'Media Type',
		isLink : false,
		mayRepeat : true
	}, {
		tag : 'snippet',
		title : 'Snippet',
		isLink : false
	}, {
		tag : 'holdingsCount',
		title : 'Holding',
		isLink : false
	}, {
		tag : 'versionCount',
		title : 'Version',
		isLink : false
	}, {
		tag : 'relevance.value',
		title : 'Score',
		isLink : false
	}, {
		tag : 'relevance.score',
		title : 'Relevance',
		isLink : false
	}, {
		tag : 'troveUrl',
		title : 'URL',
		isLink : true
	} ]
};
var ZONE_COLLECTION = {
	id : 'collection',
	holder : 'work',
	dtag : 'issued',
	rtag : 'relevance.score',
	stag : '',
	tags : [ {
		tag : 'id',
		title : 'ID',
		isLink : false
	}, {
		tag : 'title',
		title : 'Title',
		isLink : false
	}, {
		tag : 'type',
		title : 'Media Type',
		isLink : false,
		mayRepeat : true
	}, {
		tag : 'issued',
		title : 'Date Issued',
		isLink : false
	}, {
		tag : 'holdingsCount',
		title : 'Holding',
		isLink : false
	}, {
		tag : 'versionCount',
		title : 'Version',
		isLink : false
	}, {
		tag : 'relevance.value',
		title : 'Score',
		isLink : false
	}, {
		tag : 'relevance.score',
		title : 'Relevance',
		isLink : false
	}, {
		tag : 'troveUrl',
		title : 'URL',
		isLink : true
	} ]
};
var ZONE_LIST = {
	id : 'list',
	holder : 'list',
	dtag : 'date',
	rtag : 'relevance.score',
	stag : '',
	tags : [ {
		tag : 'id',
		title : 'ID',
		isLink : false
	}, {
		tag : 'title',
		title : 'Title',
		isLink : false
	}, {
		tag : 'creator',
		title : 'Creator',
		isLink : false
	}, {
		tag : 'description',
		title : 'Description',
		isLink : false
	}, {
		tag : 'listItemCount',
		title : 'Items in list',
		isLink : false
	}, {
		tag : 'identifier.type',
		title : 'Type',
		isLink : false
	}, {
		tag : 'identifier.value',
		title : 'Link',
		isLink : true
	}, {
		tag : 'relevance.value',
		title : 'Score',
		isLink : false
	}, {
		tag : 'relevance.score',
		title : 'Relevance',
		isLink : false
	}, {
		tag : 'troveUrl',
		title : 'URL',
		isLink : true
	} ]
};
var ZONE_MUSIC = {
	id : 'music',
	holder : 'work',
	dtag : 'issued',
	rtag : 'relevance.score',
	stag : '',
	tags : [ {
		tag : 'id',
		title : 'ID',
		isLink : false
	}, {
		tag : 'title',
		title : 'Title',
		isLink : false
	}, {
		tag : 'contributor',
		title : 'Contributor',
		isLink : false
	}, {
		tag : 'issued',
		title : 'Date Issued',
		isLink : false
	}, {
		tag : 'type',
		title : 'Media Type',
		isLink : false,
		mayRepeat : true
	}, {
		tag : 'snippet',
		title : 'Snippet',
		isLink : false
	}, {
		tag : 'holdingsCount',
		title : 'Holding',
		isLink : false
	}, {
		tag : 'relevence',
		title : 'Relevence',
		isLink : false
	}, {
		tag : 'versionCount',
		title : 'Version',
		isLink : false
	}, {
		tag : 'troveUrl',
		title : 'URL',
		isLink : true
	} ]
};

/**
 * Returns a structure giving the *possible* structure of a trove JSON response
 * (TROVE has no schema for these!)
 * 
 * @param zone
 * @returns
 */
function _getZoneInfo(zone) {
	var zoneInfo = null;
	switch (zone) {
	case 'newspaper':
		zoneInfo = ZONE_NEWSPAPER;
		break;
	case 'article':
		zoneInfo = ZONE_ARTICLE;
		break;
	case 'picture':
		zoneInfo = ZONE_PICTURE;
		break;
	case 'map':
		zoneInfo = ZONE_MAP;
		break;
	case 'list':
		zoneInfo = ZONE_LIST;
		break;
	case 'music':
		zoneInfo = ZONE_MUSIC;
		break;
	case 'collection':
		zoneInfo = ZONE_COLLECTION;
		break;
	case 'book':
		zoneInfo = ZONE_BOOK;
		break;
	}
	return zoneInfo;
}

/**
 * Returns all current zones as comma separated string
 */
function _getZonesAsString() {
	var zonesString = "";
	for (var i = 0; i < m_currentZones.length; i++) {
		zonesString += m_currentZones[i] + ",";
	}
	zonesString = zonesString.slice(0, -1);
	return zonesString;
}