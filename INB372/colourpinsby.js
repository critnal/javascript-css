var COLOUR_BY_HITS = 0;
var COLOUR_BY_AGE = 1;

function _getColourPinsByAgeMapButton() 
{
	return parseInt($('#map-options input[name=colourpinsby-map]:checked').val());
}

function _colourPinsByAge() 
{
	var sliderYearSpan = $(_selById(Y2K_SLIDER)).slider('value').split(';');
    var sliderYearStart = parseInt(sliderYearSpan[0]);
    var sliderYearEnd = parseInt(sliderYearSpan[1]);
	var icon = "images/ageint";
    var oldest = (_getColourPinsByAgeMapButton() == 1);
	for (var locn in m_locationsCache) {
		var year = (oldest ? m_locationsCache[locn].yMin : m_locationsCache[locn].yMax);
		var ageInt = _getAgeInt(year, sliderYearStart, sliderYearEnd);
		m_locationsCache[locn].marker.setIcon(icon + ageInt + ".png");
	}
}

/**
 * Gets an int between 1 and 10 based on the input
 * marker's year as a fraction between the slider's
 * min and max year. This is used to colour pins by age
 * using ageint#.png's.
 * @param markerYear
 * @param sliderYearStart
 * @param sliderYearEnd
 * @returns int from 1 - 10
 */
function _getAgeInt(markerYear, sliderYearStart, sliderYearEnd) 
{
	var numerator = markerYear - sliderYearStart;
	var denominator = sliderYearEnd - sliderYearStart;
	var temp = parseInt(numerator / denominator * 10) ;	
	if (temp < 0) {
		temp = 0;
	} else if (temp > 9) {
		temp = 9;
	}
	return temp + 1;
}