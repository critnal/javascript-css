/**
 * This handles advanced queries that have fields that require validation before
 * execution. If all necessary fields are valid, execute query. Otherwise,
 * display message.
 */
function _doAdvancedQuery() {
	if (_advancedQueryFieldsAreValid()) {
		m_currentQuery = _createQueryString();
		_doQuery(0);
	}
}

/**
 * If valid years have been entered into the Advanced Query min and max year
 * fields this returns a date filter to be added to the current query.
 * 
 * @returns {String}
 */
function _yearFields() {
	var dateString = '';
	m_currentMinYear = $('input#aq-year-min').val();
	m_currentMaxYear = $('input#aq-year-max').val();
	dateString = 'date:[' + m_currentMinYear + ' TO ' + m_currentMaxYear + ']';
	return dateString;
}


/**
 * Gets all the zones selected in the Advanced Query tab to search
 * 
 * @returns A string array of selected zones
 */
function _getAdvancedQueryZones() {
	var selectedZones = new Array();
	$(".zonecheckboxes input:checked").each(function() {
		selectedZones.push($(this).attr('value'));
	});
	return selectedZones;
}

/**
 * Check that advanced query fields values are valid.
 */
function _advancedQueryFieldsAreValid() {
	// Check that the query field isn't empty
	if ($('input#aq1').val().length <= 0) {
		_popupDialog(ALERT, "Please enter a search term");
		return false;
	}
	
	// Check that the year fields are valid
	if ( 
			!(
				(
					$('input#aq-year-min').val() >= m_defaultMinYear && 					
					$('input#aq-year-min').val() <= m_defaultMaxYear			
				) && (					
					$('input#aq-year-max').val() >= m_defaultMinYear && 					
					$('input#aq-year-max').val() <= m_defaultMaxYear			
				) && (					
					$('input#aq-year-max').val() >= $('input#aq-year-min').val()
				) 				
			) 
		) 
	{
		_popupDialog(ALERT, "Please enter valid years between "
				+ m_defaultMinYear + " and " + m_defaultMaxYear);
		return false;
	}
	
	// Check that at least one zone checkbox is checked
	m_currentZones = _getAdvancedQueryZones();
	if (typeof m_currentZones[0] === 'undefined' || m_currentZones[0] === null) {
		_popupDialog(ALERT, "Please select at least one type of result to search for.");
		return false;
	}
	
	return true;
}

/**
 * Reset all advanced query fields to their default values
 */
function resetAdvancedQueryPaneToDefaults() {
	m_currentMinYear = m_defaultMinYear;
	m_currentMaxYear = m_defaultMaxYear;
	
	$(".zonecheckboxes input").each(function() {
		if ($(this).attr('value') == 'newspaper') {
			$(this).prop('checked', true);
		} else {
			$(this).prop('checked', false);
		}
	});
}