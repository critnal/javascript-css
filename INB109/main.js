
var itemNames = new Array();
var itemTypes = new Array();
var itemPrices = new Array();

var sizeNames = ["Small", "Regular", "Tall"];
var sizePrices = [0, 0.50, 1.00];

var selectedItems = new Array();
var checkedItems = new Array();

function createItem(name, type, price) {
	itemNames.push(name);
	itemTypes.push(type);
	itemPrices.push(price);
}




function selectedItem(itemName, itemSize, isChecked) {
	this.itemName = itemName;
	this.itemSize = itemSize;
	this.isChecked = isChecked;
}


function addItemBar() {

	selectedItems[selectedItems.length] = new selectedItem("select", "Small", true);
	drawItemBars();
	
}
			
function drawItemBars() {

	var htmlString = "";
	for (var i = 0; i < selectedItems.length; i++) {
		htmlString += "<div class='Item' id='Item" + i + "'>";
		htmlString += "<img class='ItemBar' src='images/ItemBar.png'>";
		htmlString += "<form ><input type='checkbox' class='CheckBox' id='CheckBox " + i + "' onclick='updateCheckedItems(this)'" + getItemChecked(i) + "></form>";
		htmlString += "<select class='SelectItemBox' id='SelectItemBox" + i + "' onchange='updateSelectedItems(this)'>" + getItemNameOptions(i) + "</select>";
		if (isDrink(i)) { 
			htmlString += "<select class='SelectSizeBox' id='SelectSizeBox" + i + "' onchange='updateSelectedSizes(this)'>" + getItemSizeOptions(i) + "</select>";
		}
		htmlString += "<form action=''><input type='button' class='RemoveButton' id='RemoveButton" + i + "' onclick='removeItemBar(this)'></form>";
		htmlString += "<p class='ItemValue' id='ItemValue" + i + "'></p>";
		htmlString += "</div>";						
	}
	$(".Items").html(htmlString);
	updateValueFields();
	updateTotal();
	updatePlaceOrderBar();
}

function removeItemBar(itemBar) {
	var id = getIDNum(itemBar);
	selectedItems.splice(id, 1);
	drawItemBars();
}


	
			

function getItemNameOptions(id) {
	var returnString = "<option value='select'>Select an item</option>";
	for (var i = 0; i < itemNames.length; i++) {
		returnString += "<option value='" + itemNames[i] + "'" + itemIsSelected(id, itemNames[i]) + ">" + itemNames[i] + "</option>";
	}
	return returnString;
}

function itemIsSelected(id, option) {
	if (selectedItems[id].itemName == option) {
		return " selected";
	}
	return "";
}

function isDrink(id) {
	for (var i = 0; i < itemNames.length; i++) {
		if (itemNames[i] == selectedItems[id].itemName && itemTypes[i] == "Drink") {
			return true;
		}
	}
	return false;
}

function getItemSizeOptions(id) {
	var returnString = "";
	for (var i = 0; i < sizeNames.length; i++) {
		returnString += "<option value='" + sizeNames[i] + "'" + sizeIsSelected(id, sizeNames[i]) + ">" + sizeNames[i] + "</option>";
	}
	return returnString;
}

function sizeIsSelected(id, option) {
	if (selectedItems[id].itemSize == option) {
		return " selected";
	}
	return "";
}



function getItemChecked(id) {
	if (selectedItems[id].isChecked) {
		return " checked";
	}
	return "";
}





function updatePlaceOrderBar() {
	if (selectedItems.length > 0) {
		$(".PlaceOrderBar").attr("src", "images/PlaceOrderBarEnabled.png");
	} else {
		$(".PlaceOrderBar").attr("src", "images/PlaceOrderBarDisabled.png");
	}
}

function updateSelectedItems(selectBox) {
	var id = getIDNum(selectBox);				
	selectedItems[id].itemName = $(selectBox).val();
	
	drawItemBars();
	updateValueFields();
	updateTotal();
}

function updateSelectedSizes(selectBox) {
	var id = getIDNum(selectBox);
	selectedItems[id].itemSize = $(selectBox).val();
	
	updateValueFields();
	updateTotal();
}

function updateCheckedItems(checkBox) {
	var id = getIDNum(checkBox);
	selectedItems[id].isChecked = checkBox.checked;
	
	updateTotal();
}

function updateValueFields() {
	for (var i = 0; i < selectedItems.length; i++) {
		if (selectedItems[i].itemName == "select") {
			$("#ItemValue" + i).text("");
		} else {
			var selectedItemIndex = itemNames.indexOf(selectedItems[i].itemName);
			var selectedItemPrice = itemPrices[selectedItemIndex];
			
			if (isDrink(i)) {
				selectedItemPrice += sizePrices[sizeNames.indexOf(selectedItems[i].itemSize)];
			}
			
			$("#ItemValue" + i).text("$" + selectedItemPrice);
		}
	}
}

function updateTotal() {
	var total = 0;
	for (var i = 0; i < selectedItems.length; i++) {
		if (selectedItems[i].itemName != "select" && selectedItems[i].isChecked) {
			total += itemPrices[itemNames.indexOf(selectedItems[i].itemName)];	
			if (isDrink(i)) {
				total += sizePrices[sizeNames.indexOf(selectedItems[i].itemSize)];
			}
		}					
	}
	if (total == 0) {
		$(".TotalValue").text("$0");
	} else {
	$(".TotalValue").text("$"+total);
	}
}

function getIDNum(element) {
	return element.id.slice(element.id.length - 1);
}



$(document).ready( function(){
	$(".AddItem").on("click", function() {
		addItemBar();
	});
	
	$(".PlaceOrderBar").on("click", function() {
		alert("Order placed for " + $("#PickUpTime").val());
	});
});			

